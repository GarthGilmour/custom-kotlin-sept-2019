package rochambeau.start

import java.util.Random

fun main(args: Array<String>) {
    println("Lets play Rock / Paper / Scissors")

    val random = Random()
    val programChoice = random.nextInt(3)
    println("Program chose $programChoice")
}