package add.vs.plus

fun main() {
    val list1 = mutableListOf("ab", "cd", "ef")
    val list2 = list1 + "ghij"
    list1.add("klmn")

    println(list1)
    println(list2)

}