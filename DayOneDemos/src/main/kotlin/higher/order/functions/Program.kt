package higher.order.functions

fun wrapInH1(text: String) = "<h1>$text</h1>"
fun wrapInH2(text: String) = "<h2>$text</h2>"
fun wrapInTag(tagName: String,
              text: String): String {
    val openingTag = "<$tagName>"
    val closingTag = "</$tagName>"
    return "$openingTag$text$closingTag"
}

fun buildWrapper(tagName: String): (String) -> String {
    val openingTag = "<$tagName>"
    val closingTag = "</$tagName>"

    return { text -> "$openingTag$text$closingTag" }
}

fun main() {
    println(wrapInH1("Homer"))
    println(wrapInH2("Marge"))
    println(wrapInTag("h3", "Bart"))

    val wrapInMark = buildWrapper("mark")
    val wrapInDiv = buildWrapper("div")
    println(wrapInMark("Maggie"))
    println(wrapInDiv("Liza"))
    println(wrapInDiv("Snowball III"))
}