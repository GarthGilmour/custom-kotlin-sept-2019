package lambda.with.receiver

import java.time.LocalDate

fun demoA(input1: String,
          input2: LocalDate,
          input3: String.(LocalDate) -> Unit) = input1.input3(input2)

fun demoB(input1: String,
          input2: LocalDate,
          input3: LocalDate.(String) -> Unit) = input2.input3(input1)

fun main() {
    val tmp1 = "abcDEF"
    val tmp2 = LocalDate.now()

    demoA(tmp1, tmp2) {
        println(this.toUpperCase())
        println(toUpperCase())
        println(it.year)
    }

    demoB(tmp1, tmp2) {
        println(it.toUpperCase())
        println(this.year)
        println(year)
    }
}