package writing.filter.by.hand

fun<T> myFilter(input: List<T>,
             predicate: (T) -> Boolean): List<T> {
    val results = mutableListOf<T>()
    for(item in input) {
        if(predicate(item)) {
            results.add(item)
        }
    }
    return results
}

fun<T, U> myMap(input: List<T>,
             action: (T) -> U): List<U> {
    val results = mutableListOf<U>()
    for(item in input) {
        results.add(action(item))
    }
    return results
}

fun<T> myReduce(input: List<T>,
                action: (T, T) -> T): T {
    var tmpResult = action(input[0], input[1])
    for(index in 2 until input.size) {
        tmpResult = action(tmpResult, input[index])
    }
    return tmpResult
}

fun<T, U> myFold(input: List<T>,
                initialValue: U,
                action: (U, T) -> U): U {
    var tmpResult = initialValue
    for(item in input) {
        tmpResult = action(tmpResult, item)
    }
    return tmpResult
}

fun main() {
    val data = listOf("ab","cde","fghi","jk",
        "lmn","opqr","st","uvw","xyz")

    println("Custom filter method")
    val result1 = myFilter(data) { it.length == 3 }
    result1.forEach(::println)

    println("Custom map method")
    val result2 = myMap(data) { it.length }
    result2.forEach(::println)

    println("Custom reduce method")
    val result3 = myReduce(data) { a, b -> "$a $b" }
    println(result3)

    println("Custom fold method")
    val result4 = myFold(data, 1000) { a, b -> a + b.length }
    println(result4)
}