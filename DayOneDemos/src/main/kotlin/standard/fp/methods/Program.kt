package standard.fp.methods

fun main() {
    fun concat(s1: String, s2: String) = "$s1-$s2"

    val data = listOf("ab","cde","fghi","jk",
                      "lmn","opqr","st","uvw","xyz")

    println("Demo filter")
    data.filter { it.length == 3 }
        .forEach(::printTabbed)

    println("Demo map")
    data.map { it.length }
        .forEach(::printTabbed)

    println("Demo reduce")
    val result1 = data.reduce { str1, str2 ->
        str1 + str2
    }
    printTabbed(result1)

    println("Demo fold")
    val result2 = data.fold("--") { str1, str2 ->
        str1 + str2
    }
    printTabbed(result2)

    println("Demo fold with named function")
    val result3 = data.fold("--", ::concat)
    printTabbed(result3)

    println("Demo partition")
    val (shortStrings, longStrings) = data.partition { it.length < 3 }
    println("Short strings")
    shortStrings.forEach(::printTabbed)
    println("Long strings")
    longStrings.forEach(::printTabbed)
}

fun printTabbed(input: Any) = println("\t$input")