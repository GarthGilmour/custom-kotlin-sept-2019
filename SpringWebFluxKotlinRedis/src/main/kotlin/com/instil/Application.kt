package com.instil

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringWebFluxKotlinRedisApplication

fun main(args: Array<String>) {
	runApplication<SpringWebFluxKotlinRedisApplication>(*args)
}
