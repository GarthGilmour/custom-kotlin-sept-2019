package com.instil.controllers

import com.instil.DeletionException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

import org.springframework.http.HttpStatus.BAD_REQUEST
import reactor.core.publisher.Mono

@ControllerAdvice
@RestController
class ExceptionResolver {
    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(value = [DeletionException::class])
    fun deleteError(ex: Exception)= Mono.just("""
            {
                "errorType": "${ex.javaClass.simpleName}",
                "message": "${ex.message}"
            }
        """.trimIndent())
}
