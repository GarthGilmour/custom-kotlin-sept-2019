package basics.casting

import java.time.LocalTime

class Person(val name: String)

fun doDemo(input: Any) {
    val result = when (input) {
        is String -> input.toUpperCase()
        is LocalTime -> input.hour
        is Person -> input.name
        else -> input.toString()
    }
    println(result)
}
fun main(args: Array<String>) {
    doDemo("wibble")
    doDemo(LocalTime.NOON)
    doDemo(Person("Dave"))
    doDemo(123)
}
