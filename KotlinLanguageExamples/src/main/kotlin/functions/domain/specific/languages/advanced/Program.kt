package functions.domain.specific.languages.advanced

enum class CourseType {
    Beginner, Intermediate, Advanced
}

interface Node {
    fun render(indent: String = ""): String
}

class Attribute(val name: String, val value: Any) : Node {
    override fun render(indent: String) = "$name=\"$value\""
    override fun toString() = render()
}

abstract class Element(private val tagName: String) : Node {
    val attributes = mutableListOf<Attribute>()
    protected val openTag: String
        get() {
            fun spacer() = if (attributes.isEmpty()) "" else " "
            return attributes.joinToString(prefix = "<$tagName${spacer()}",
                                           separator = " ",
                                           postfix = ">")
        }
    protected val closeTag: String
        get() = "</$tagName>"

    override fun toString() = render()
}

open class LeafElement(tagName: String,
                       private val value: String) : Element(tagName) {
    override fun render(indent: String): String {
        return "$indent$openTag$value$closeTag\n"
    }
}

open class ParentElement(tagName: String) : Element(tagName) {
    private val content = mutableListOf<Node>()

    operator fun String.not() = content.add(Title(this))
    operator fun String.unaryPlus() = content.add(Item(this))


    open fun<T : Node> builder(obj: T,
                               inputFunc: T.() -> Unit,
                               attrs: List<Attribute> = emptyList()) {
        content.add(obj)
        attributes.addAll(0, attrs)
        obj.inputFunc()
    }

    override fun render(indent: String): String {
        val builder = StringBuilder()
        builder.append("$indent$openTag\n")
        content.forEach { builder.append(it.render(indent.plus("  "))) }
        builder.append("$indent$closeTag\n")
        return builder.toString()
    }
}

class Description : ParentElement("description")
class Module : ParentElement("module")
class Title(value: String) : LeafElement("title", value)
class Item(value: String) : LeafElement("item", value)

class Course : ParentElement("course") {
    fun description(type: CourseType = CourseType.Beginner,
                    duration: Int = 3,
                    func: Description.() -> Unit) {
        val description = Description()
        with(description.attributes) {
            add(Attribute("type", type))
            add(Attribute("duration", duration))
        }
        builder(description, func)
    }
    fun modules(func: ModuleList.() -> Unit) = builder(ModuleList(), func)
}

class ModuleList : ParentElement("moduleList") {
    fun module(func: Module.() -> Unit) = builder(Module(), func)
}

fun course(id: String, inputFunc: Course.() -> Unit) = Course().apply {
    attributes.add(Attribute("id", id))
    inputFunc()
}

fun main(args: Array<String>) {
    val course = course("AB12") {
        description(duration = 4) {
            !"Kotlin Programming"
        }
        modules {
            module {
                !"Object Orientation"
                +"Classes"
                +"Interfaces"
                +"Methods"
            }
            module {
                !"Functional Programming"
                +"Lambdas"
                +"Reductions"
                +"Currying"
            }
            module {
                !"Java Interoperability"
                +"Kotlin to Java"
                +"Java to Kotlin"
            }
        }
    }
    println(course)
}