package demos.kotlin.coroutines.async

import demos.kotlin.coroutines.shared.pingSlowServer
import kotlinx.coroutines.experimental.*

fun main(args: Array<String>) = runBlocking<Unit> {
    println("Program starts")

    val deferredJobs = mutableListOf<Deferred<String>>()
    deferredJobs += async { pingSlowServer(8) }
    deferredJobs += async { pingSlowServer(6) }
    deferredJobs += async { pingSlowServer(4) }
    deferredJobs += async { pingSlowServer(2) }

    val results = deferredJobs
                         .map { it.await() }
                         .joinToString(
                                 prefix = "\"",
                                 postfix = "\"",
                                 separator = ", " )
    println("Results are $results")
    println("Program complete")
}



