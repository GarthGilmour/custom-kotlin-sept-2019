package demos.kotlin.coroutines.shared

import javax.ws.rs.client.Client
import javax.ws.rs.client.ClientBuilder
import javax.ws.rs.core.MediaType

suspend fun pingSlowServer(timeout: Int): String {
    val client = ClientBuilder.newClient()
    val result = client
            .target("http://localhost:8080")
            .path("ping")
            .path(timeout.toString())
            .request(MediaType.APPLICATION_JSON)
            .get(Array<String>::class.java)

    return result?.get(0) ?: "No Response"
}