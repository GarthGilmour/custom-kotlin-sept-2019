package demos.kotlin.coroutines.methods

import demos.kotlin.coroutines.shared.pingSlowServer
import kotlinx.coroutines.experimental.*

fun pingSlowServerAsync(timeout: Int) = async {
    pingSlowServer(timeout)
}

fun main(args: Array<String>) = runBlocking<Unit> {
    println("Program starts")

    val deferredJobs = mutableListOf<Deferred<String>>()
    deferredJobs += pingSlowServerAsync(8)
    deferredJobs += pingSlowServerAsync(6)
    deferredJobs += pingSlowServerAsync(4)
    deferredJobs += pingSlowServerAsync(2)

    val results = deferredJobs
                         .map { it.await() }
                         .joinToString(
                                 prefix = "\"",
                                 postfix = "\"",
                                 separator = ", " )
    println("Results are $results")
    println("Program complete")
}



