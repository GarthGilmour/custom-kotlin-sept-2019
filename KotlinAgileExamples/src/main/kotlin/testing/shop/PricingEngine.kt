package testing.shop

interface PricingEngine {
    fun price(itemNo: String, quantity: Int): Double
}