package testing.cell

import io.kotlintest.matchers.shouldBe
import io.kotlintest.matchers.shouldThrow
import io.kotlintest.specs.ShouldSpec
import testing.cell.CellStates.Alive
import testing.cell.CellStates.Dead

class CellTest : ShouldSpec() {
    override val oneInstancePerTest = true
    private val neighbours = mutableListOf<GameOfLifeCell>()
    private fun populateNeighbours() {
        neighbours.clear()
        for (x in 1..8) {
            neighbours.add(GameOfLifeCell(emptyList()))
        }
    }
    init {
        populateNeighbours()
        should("throws when it has too many neighbours") {
            val exception = shouldThrow<CellStateException> {
                neighbours.add(GameOfLifeCell(emptyList()))
                GameOfLifeCell(neighbours)
            }
        }
        should("be dead by default") {
            val cell = GameOfLifeCell(neighbours)
            cell.state shouldBe Dead
        }
        should("be able to become alive") {
            val cell = GameOfLifeCell(neighbours)
            cell.makeAlive()
            cell.state shouldBe Alive
        }
        should("be able to become dead") {
            val cell = GameOfLifeCell(neighbours)
            cell.makeAlive()
            cell.state shouldBe Alive
            cell.makeDead()
            cell.state shouldBe Dead
        }
        should("become alive when three neighbours are alive") {
            val cell = GameOfLifeCell(neighbours)
            neighbours[0].makeAlive()
            neighbours[1].makeAlive()
            neighbours[2].makeAlive()
            cell.changeState()
            cell.state shouldBe Alive
        }
        should("starve when less than two neighbours are alive") {
            val cell = GameOfLifeCell(neighbours)
            cell.makeAlive()
            neighbours[1].makeAlive()
            cell.changeState()
            cell.state shouldBe Dead
        }
        should("be overcrowded when more than three neighbours are alive") {
            val cell = GameOfLifeCell(neighbours)
            cell.makeAlive()
            neighbours[0].makeAlive()
            neighbours[1].makeAlive()
            neighbours[2].makeAlive()
            neighbours[3].makeAlive()
            cell.changeState()
            cell.state shouldBe Dead
        }
        should("stays dead unless three neighbours are alive") {
            for (x in 0..8) {
                val cell = GameOfLifeCell(neighbours)
                for (y in 0 until x) {
                    neighbours[y].makeAlive()
                }
                cell.changeState()
                if (x != 3) {
                    cell.state shouldBe Dead
                }
            }
        }
    }
}