package testing.cell

class CellStateException(msg: String) : Throwable(msg)