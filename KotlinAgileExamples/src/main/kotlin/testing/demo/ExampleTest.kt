package testing.demo

import io.kotlintest.TestCaseContext
import io.kotlintest.specs.ShouldSpec

class ExampleTest : ShouldSpec() {
    private val items = mutableListOf<String>()
    private var thing = 123

    private fun address(input: Any): String {
        return input.toString()
                .replaceBefore('@',"")
                .removePrefix("@")
    }

    override fun interceptTestCase(context: TestCaseContext, test: () -> Unit) {
        println("Object running intercept method is at ${ address(this) }")
        println("\tBefore test ${items.size} and $thing")
        with(context.spec as ExampleTest) {
            items.add("foo")
            thing = 456
            test()

        }
        println("\tAfter test ${items.size} and $thing")
    }

    init {
        should("first test") {
            println("Object running first test is at ${ address(this) }")
            println("\tDuring test ${items.size} and $thing")
        }
        should("second test") {
            println("Object running second test is at ${ address(this) }")
            println("\tDuring test ${items.size} and $thing")
        }
    }
}